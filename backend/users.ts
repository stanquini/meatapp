export class User {
  constructor(public email: string,
              public name: string,
              private password: string){}

  matches(another: User): boolean {
    return another !== undefined &&
     another.email === this.email &&
     another.password === this.password
  }
}

export const users: {[key:string]: User} = {
  "stanquini@gmail.com": new User('stanquini@gmail.com', 'stanquini', 'fred123'),
  "camila@gmail.com": new User('camila@gmail.com', 'Camila', 'camila123')
}
