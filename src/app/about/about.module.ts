import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './about.component';
//LAZY LOADING
const ROUTES: Routes = [
  {path: '', component: AboutComponent}
]
//LAZY LOADING
@NgModule({
  declarations: [AboutComponent],
  imports: [RouterModule.forChild(ROUTES)]
})

export class AboutModule {}