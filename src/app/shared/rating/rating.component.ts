import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mt-rating',
  templateUrl: './rating.component.html'
})
export class RatingComponent implements OnInit {

  @Output() rated = new EventEmitter<number>()
  
  rates: number[] = [1,2,3,4,5] 

  rate: number = 0

  previoursRate: number

  constructor() { }

  ngOnInit() {}

  setRate(r: number) {
    
    this.rate = r
    this.previoursRate = undefined
    this.rated.emit(this.rate)
  }

  setTemporaryRate(r: number){

    if (this.previoursRate === undefined) {
      this.previoursRate = this.rate
    }
    this.rate = r
  }

  clearTemporaryRate(){
    if (this.previoursRate !== undefined) {
      this.rate = this.previoursRate
      this.previoursRate = undefined
    }
  }

}
