import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RatingComponent } from './rating/rating.component';
import { RadioComponent } from './radio/radio.component';
import { InputComponent } from './input/input.component';
import { ShoppingCartService } from "../restaurant-detail/shopping-cart/shopping-cart.service";
import { RestaurantService } from "../restaurants/restaurants.service";
import { OrderService } from "../order/order.service";
import { SnackbarComponent } from '../shared/messages/snackbar/snackbar.component';
import { NotificationService } from './messages/notification.service';
import { LoginService } from './../security/login/login.service';
import { LoggedinGuard } from '../security/loggedin.guard';


//MODULO COMPARTILHADO
@NgModule({
  declarations: [InputComponent, RadioComponent, RatingComponent, SnackbarComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [InputComponent, RadioComponent, SnackbarComponent, RatingComponent, CommonModule, FormsModule, ReactiveFormsModule]
})

export class SharedModule {
  
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ShoppingCartService,
                  RestaurantService,
                  OrderService,
                  NotificationService,
                  LoginService,
                  LoggedinGuard]
    }
  }
}