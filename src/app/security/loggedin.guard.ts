import { Injectable } from "@angular/core";
import { CanLoad, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";

import { LoginService } from "./login/login.service";

@Injectable()
export class LoggedinGuard implements CanLoad, CanActivate{

    constructor(private loginService: LoginService){}
    
    checkAuthentication(path: string): boolean {
        
        const loggedIn = this.loginService.isLoggedIn()

        if(!loggedIn) {
            this.loginService.handleLogin(`/${path}`)
        }

        return loggedIn
    }

    
    
    canLoad(route: Route): boolean {
        
        return this.checkAuthentication(route.path)
    }

    //ActivatedRouteSnapshot copia da rota que foi ativada
    //RouterStateSnapshot arvore de ActivatedRouteSnapshot ele tem todos caminhos q foram passadas para chegar na rota
    canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): boolean {
        
        return this.checkAuthentication(activatedRoute.routeConfig.path)
    }
}