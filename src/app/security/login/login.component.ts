import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { NotificationService } from '../../shared/messages/notification.service';
import { LoginService } from './login.service';

@Component({
  selector: 'mt-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  navigateTo: string

  constructor(private fb: FormBuilder,
              private loginService: LoginService, 
              private noticationService: NotificationService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', [Validators.required])
    })
    this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/')
  }

  login() {
    this.loginService.login(this.loginForm.value.email,
                            this.loginForm.value.password)//subscribe - 3 callbacks primeiro - sucesso , segundo callback de erro, terceiro quando o Observable terminar
      .subscribe(
                user => this.noticationService.notify(`Bem vindo, ${user.name}`),
                
                response =>//type of response is HttpError Response
                  this.noticationService.notify(response.error.message),
                
                () => this.router.navigate([atob(this.navigateTo)])
                )
  }

}
